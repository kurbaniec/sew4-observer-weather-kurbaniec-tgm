package observer.weather.DisplayFX;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import observer.weather.WeatherData;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Controller used to modify {@link FXApplication} on the fly.
 * <br>
 * Based on <a href="https://bitbucket.org/kurbaniec/sew4-simple-chat-kurbaniec-tgm/src/master/">SimpleChat</a>.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.13 v1
 */
public class Controller {

    private WeatherData weatherData;

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @FXML
    private ListView listView;

    @FXML
    private VBox vbox;


    public void initialize() {

    }

    public void stop() {
        Platform.exit();
    }


    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    public void addLabel(String text) {
        //Platform.runLater(() -> textArea.appendText("\n" + text));
        Platform.runLater(() -> {
            Label label = new Label(text);
            label.setWrapText(true);
            vbox.getChildren().add(label);
        });
    }

    public void addDisplay(String user) {
        Platform.runLater(() -> {
            ObservableList list = listView.getItems();
            if(!list.contains(user)) {
                list.add(user);
            }
        });
    }
}
